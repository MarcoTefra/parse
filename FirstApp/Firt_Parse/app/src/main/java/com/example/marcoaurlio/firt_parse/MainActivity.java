package com.example.marcoaurlio.firt_parse;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Parse.enableLocalDatastore(this);

        Parse.initialize(this);

        Button btGravar;
     //   Button btCarregar;
        final EditText edNome;
        final EditText edIdade;
        final EditText edEndereco;
        final EditText edTelefone;
     //   final TextView txCarrega;

        btGravar = (Button)findViewById(R.id.button);
     //   btCarregar = (Button)findViewById(R.id.buttonCarregar);
        edNome = (EditText)findViewById(R.id.editTextNome);
        edIdade = (EditText)findViewById(R.id.editTextIdade);
        edEndereco = (EditText)findViewById(R.id.editTextEndereco);
        edTelefone = (EditText)findViewById(R.id.editTextTelefone);
     //   txCarrega = (TextView)findViewById(R.id.textView6);

        btGravar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome = edNome.getText().toString();
                String idade = edIdade.getText().toString();
                String end = edEndereco.getText().toString();
                String telefone = edTelefone.getText().toString();

                ParseObject testObject = new ParseObject("TestObject");
                testObject.put("nome", nome);
                testObject.put("idade", idade);
                testObject.put("endereco", end);
                testObject.put("telefone", telefone);

               // String auxNome = testObject.get(nome).toString();

              //  String auxNome = testObject.getString("nome");



                testObject.saveInBackground();

                edNome.setText("");
                edIdade.setText("");
                edEndereco.setText("");
                edTelefone.setText("");


               // edNome.setText(auxNome);
            }
        });

      /*  btCarregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseObject testObject = new ParseObject("TestObject");
                //String auxNome = testObject.getString("nome");

               // List<String> lista;

               // lista = testObject.getList("nome");
               // String aux = String.valueOf(lista.size());

                try {
                    testObject.fetchFromLocalDatastore();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String auxNome = testObject.getString("nome");

                txCarrega.setText("*** " + auxNome);
            }
        });*/

       // edNome = (EditText)findViewById(R.id.editText);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // [Optional] Power your app with Local Datastore. For more info, go to
// https://parse.com/docs/android/guide#local-datastore
       /* Parse.enableLocalDatastore(this);

        Parse.initialize(this);

        ParseObject testObject = new ParseObject("TestObject");
        testObject.put("nome", "Maria");
        testObject.saveInBackground();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
